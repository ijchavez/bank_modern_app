import style from '../style'
import { discount } from '../assets'

// eslint-disable-next-line react/prop-types
const Discount = ({ discountAmount, discountPeriod }) => {
  return (
    <>
        <div className="flex flex-row items-center py-[6px] px-4 bg-discount-gradient rounded-[10px] mb-2">
        <img src={ discount } alt="discount" className="w-[32px] h-[32px] " />
            <p className={`ml-2 ${style.paragraph}`}>
                <span className="text-white">{discountAmount} </span>
                Discount for {" "}
                <span className="text-white">{discountPeriod} </span>
                Account
            </p>
        </div>
    </>

  )
}

export default Discount