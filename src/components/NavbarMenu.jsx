import { navLinks } from '../constants'
// eslint-disable-next-line react/prop-types
const NavbarMenu = ( { ulClassName, liClassName, mrIf, mrElse } ) => {
  return (
    <>
        <ul className={`${ulClassName}`}>
            {navLinks.map((nav, index) =>  (
                <li key={nav.id}
                    className={`${liClassName}
                               ${index === navLinks.length - 1 ? mrIf : mrElse} text-white mr-10`}>
                        <a href={`#${nav.id}`}>
                            {nav.title}
                        </a>

                </li>
            ))}
        </ul>
    </>
  )
}

export default NavbarMenu