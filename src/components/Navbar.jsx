import { useState } from 'react'
import NavbarMenu from './NavbarMenu'
import { close, logo, menu } from '../assets'

const Navbar = () => {
  const [toggle, setToggle] = useState(false)
  return (
    <nav className="w-full flex py-6 justify-between items-center navbar">
        <img src={logo} alt="hoobank"
             className='w-[124px] h-[32px]' />
        <NavbarMenu ulClassName='list-none sm:flex hidden justify-end items-center flex-1'
                    liClassName='font-poppins font-normal cursor-pointer text-[16px] '
                    mrIf='mr-0'
                    mrElse='mr-10' />
        <div className="sm:hidden flex flex-1 justify-end items-center">
            <img src={ toggle ? close : menu } 
                 alt="menu"
                 className='w-[28px] h-[28px] object-contain'
                 onClick={() => setToggle((prev) => !prev)} />
            <div className={`${toggle ? 'flex' : 'hidden'} p-6 bg-black-gradient absolute top-20 right-0 mx-4 my-2 min-w-[140px] rounded-xl sidebar`}>
                <NavbarMenu ulClassName='list-none flex flex-col justify-end items-center flex-1'
                            liClassName='font-poppins font-normal cursor-pointer text-[16px] '
                            mrIf='mr-0'
                            mrElse='mb-4' />
            </div>
        </div>
    </nav>
  ) 
}

export default Navbar